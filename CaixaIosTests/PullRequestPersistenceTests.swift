//
//  PullRequestPersistenceTests.swift
//  CaixaIos
//
//  Created by Joao Bosco on 14/04/17.
//  Copyright © 2017 Joao Bosco. All rights reserved.
//

import XCTest

@testable import CaixaIos

class PullRequestPersistenceTests: XCTestCase {
    
    func testingGetRequests() {
        
        var requests = [PullRequest]()
        let expec = expectation(description: "Request return")
        
        PullRequestHandler.requestsFrom(login: "elastic", name: "elasticsearch"){ (result: [PullRequest]) in
            requests = result
            expec.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(requests.count > 0)
        }
    }
    
}
