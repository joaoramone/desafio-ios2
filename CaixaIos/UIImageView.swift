//
//  UIImageView.swift
//  CaixaIos
//
//  Created by Joao Bosco on 25/12/16.
//  Copyright © 2017 Joao Bosco. All rights reserved.
//

import UIKit

extension UIImageView  {
    
    func roundToCircle(){
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = self.bounds.width/2
        self.layer.masksToBounds = true
    }
    
}
